package utils

import (
	"fmt"
	"log"

	"gopkg.in/yaml.v3"
)

func PrettyPrint(data interface{}) {
	out, err := yaml.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(out))
}

func PrettyPrintTitle(title string, data interface{}) {
	fmt.Println("================= " + title + " =================")
	PrettyPrint(data)
}

func PrintTitle(title string, data interface{}) {
	fmt.Println("================= " + title + " =================")
	fmt.Println(data)
}
