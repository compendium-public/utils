package utils

import (
	"fmt"
	"os"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"golang.org/x/crypto/ssh"
	ssh2 "gopkg.in/src-d/go-git.v4/plumbing/transport/ssh"
)

func CloneRepo(url, branch, key, workspace string) error {
	clone := false
	var TLSInsecure bool

	if os.Getenv("TLSInsecure") == "TRUE" {
		TLSInsecure = true
		fmt.Println("*** TLSInsecure == TRUE ***")
	}

	var authMethod transport.AuthMethod
	var err error

	if key != "" {
		if strings.HasPrefix(url, "http") {
			tokenFields := strings.Split(key, ":")
			if len(tokenFields) == 2 {
				authMethod = &http.BasicAuth{
					Username: tokenFields[0], // yes, this can be anything except an empty string
					Password: tokenFields[1],
				}
			}
		} else {
			signer, err := ssh.ParsePrivateKey([]byte(key))
			authMethod = &ssh2.PublicKeys{
				User:   "git",
				Signer: signer,
				HostKeyCallbackHelper: ssh2.HostKeyCallbackHelper{
					HostKeyCallback: ssh.InsecureIgnoreHostKey(),
				},
			}

			if err != nil {
				return err
			}
		}
	}

	repo, err := git.PlainOpen(workspace)
	if err != nil {
		clone = true
	} else {
		c, err := repo.Storer.Config()
		if err != nil {
			return err
		}

		if c.Remotes["origin"].URLs[0] != url {
			clone = true
		} else {
			head, err := repo.Head()
			if err != nil {
				return err
			}

			if head.Name().String() != fmt.Sprintf("refs/heads/%s", branch) {
				clone = true
			} else {

				// Get the working directory for the repository
				w, err := repo.Worktree()
				if err != nil {
					clone = true
				} else {
					// Pull the latest changes from the origin remote and merge into the current branch
					err = w.Pull(&git.PullOptions{
						Auth:          authMethod,
						ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branch)),
						//Depth:        1,
						SingleBranch:    true,
						Progress:        os.Stdout,
						RemoteName:      "origin",
						InsecureSkipTLS: TLSInsecure,
					})

					if err != nil {
						if err == git.NoErrAlreadyUpToDate {
							//
						} else if err == git.ErrNonFastForwardUpdate {
							//
						} else {
							clone = true
						}
					}
				}
			}
		}
	}

	if clone {
		err := os.RemoveAll(workspace)
		if err != nil {
			return err
		}

		repo, err := git.PlainClone(workspace, false, &git.CloneOptions{
			Auth:          authMethod,
			URL:           url,
			ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branch)),
			//Depth:         1,
			SingleBranch:    true,
			Progress:        os.Stdout,
			InsecureSkipTLS: TLSInsecure,
		})

		if err != nil {
			return err
		}

		_, err = repo.Head()
		if err != nil {
			return err
		}
	}

	return nil
}
