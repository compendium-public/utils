package main

import (
	"gitlab.com/compendium-public/utils"
)

func main() {
	log := utils.NewLogger("test")

	log.Info("Loading test.yaml")
	var data map[string]interface{}
	// err := utils.ReadYaml("test.yaml", &data)
	// log.HasErr(err)
	err := utils.YamlFileTemplateMerge([]string{"test.yaml", "test2.yaml"}, nil, &data)
	log.HasErr(err)
	utils.PrettyPrint(data)

	err = utils.WriteJson("test.json", data)
	log.HasErr(err)

	out, err := utils.ExecCommand("./", "pwd", nil, nil)
	log.HasErr(err)

	utils.PrettyPrint(out)

	utils.ExecSetDefaultNoOut()
	out, graph, providers, plan, output, state, err := utils.TerraformApply("./", "", nil)
	log.HasErr(err)
	utils.PrintTitle("graph", graph)
	utils.PrettyPrintTitle("providers", providers)
	utils.PrettyPrintTitle("plan", plan)
	utils.PrettyPrintTitle("output", output)
	utils.PrettyPrintTitle("state", state)
	utils.PrettyPrintTitle("out", out)

	out, err = utils.TerraformDestroy("./", nil)
	log.HasErr(err)
	utils.PrettyPrintTitle("out", out)

	log.Debug("debug")
	utils.GlobalLoggerSetLevelDebug()
	log.Debug("debug")
	log.Info("info")
	log.Warn("warn")
	log.Error("error")

	err = utils.CloneRepo("https://gitlab.com/compendium-public/prototype/gin", "master", "", "./repo")
	log.HasErr(err)
}
