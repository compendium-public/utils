package utils

import (
	//"encoding/json"
	json "github.com/json-iterator/go"
)

func TerraformApply(path, workspace string, tfvars map[string]string) (log, graph string, providers, plan, output, state interface{}, err error) {
	out, err := TerraformInit(path)
	log = log + "\n" + out
	if err != nil {
		return
	}

	out, providers, err = TerraformProviders(path)
	log = log + "\n" + out
	if err != nil {
		return
	}

	out, err = TerraformWorkspace(path, workspace)
	log = log + "\n" + out
	if err != nil {
		return
	}

	out, plan, err = TerraformPlan(path, tfvars)
	log = log + "\n" + out
	if err != nil {
		return
	}

	graph, err = TerraformGraph(path)
	log = log + "\n" + out
	if err != nil {
		return
	}

	out, output, state, err = TerraformApplyPlan(path)
	log = log + "\n" + out
	if err != nil {
		return
	}

	return
}

func TerraformInit(path string) (out string, err error) {
	// Run Terraform Init
	out, err = ExecCommand(
		path,
		"terraform",
		[]string{"init", "-no-color"},
		nil,
	)
	return
}

func TerraformProviders(path string) (out string, providers interface{}, err error) {
	// Generate Providers Report
	out, err = ExecCommand(
		path,
		"terraform",
		[]string{"providers", "schema", "-json"},
		nil,
	)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(out), &providers)
	if err != nil {
		return
	}
	return
}

func TerraformWorkspace(path, workspace string) (out string, err error) {
	// Create and Select Terraform Workspace
	if workspace != "" {
		var out2 string
		// Create Terraform Workspace
		out, _ = ExecCommand(
			path,
			"terraform",
			[]string{"workspace", "-no-color", "new", workspace},
			nil,
		)

		// Select Terraform Workspace
		out2, err = ExecCommand(
			path,
			"terraform",
			[]string{"workspace", "-no-color", "select", workspace},
			nil,
		)
		out = out + "\n" + out2
		if err != nil {
			return
		}
	}
	return
}

func TerraformPlan(path string, tfvars map[string]string) (out string, plan interface{}, err error) {
	// Run Terraform Plan
	PlanArgs := []string{"plan", "-out=plan.raw", "-no-color"}
	for k, v := range tfvars {
		PlanArgs = append(PlanArgs, "-var")
		PlanArgs = append(PlanArgs, k+"="+v)
	}

	_, err = ExecCommand(
		path,
		"terraform",
		PlanArgs,
		nil,
	)
	if err != nil {
		return
	}

	// Generate Report from Plan
	out, err = ExecCommand(
		path,
		"terraform",
		[]string{"show", "-json", "plan.raw"},
		nil,
	)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(out), &plan)
	if err != nil {
		return
	}
	return
}

func TerraformPlanPlain(path string, tfvars map[string]string) (out string, plan interface{}, err error) {
	// Run Terraform Plan
	PlanArgs := []string{"plan", "-out=plan.raw", "-no-color"}
	for k, v := range tfvars {
		PlanArgs = append(PlanArgs, "-var")
		PlanArgs = append(PlanArgs, k+"="+v)
	}

	out, err = ExecCommand(
		path,
		"terraform",
		PlanArgs,
		nil,
	)
	if err != nil {
		return
	}

	// Generate Report from Plan
	jsonout, err := ExecCommand(
		path,
		"terraform",
		[]string{"show", "-json", "plan.raw"},
		nil,
	)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(jsonout), &plan)
	if err != nil {
		return
	}

	out, err = ExecCommand(
		path,
		"terraform",
		[]string{"show", "plan.raw"},
		nil,
	)
	if err != nil {
		return
	}

	return
}

func TerraformGraph(path string) (out string, err error) {
	// Generate Report from Graph
	out, err = ExecCommand(
		path,
		"terraform",
		[]string{"graph", "-no-color"},
		nil,
	)
	if err != nil {
		return
	}
	return
}

func TerraformApplyPlan(path string) (out string, output, state interface{}, err error) {
	// Run Terraform apply
	out, err = ExecCommand(
		path,
		"terraform",
		[]string{"apply", "-auto-approve", "-input=false", "plan.raw", "-no-color"},
		nil,
	)
	if err != nil {
		return
	}

	// Generate Report from Output
	outputOut, err := ExecCommand(
		path,
		"terraform",
		[]string{"output", "-json"},
		nil,
	)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(outputOut), &output)
	if err != nil {
		return
	}

	// Generate Report from State
	stateOut, err := ExecCommand(
		path,
		"terraform",
		[]string{"state", "pull"},
		nil,
	)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(stateOut), &state)
	if err != nil {
		return
	}

	return
}

func TerraformDestroy(path string, tfvars map[string]string) (out string, err error) {
	PlanArgs := []string{"destroy", "-auto-approve", "-input=false", "-no-color"}
	for k, v := range tfvars {
		PlanArgs = append(PlanArgs, "-var")
		PlanArgs = append(PlanArgs, k+"="+v)
	}

	// Run Terraform destroy
	out, err = ExecCommand(
		path,
		"terraform",
		PlanArgs,
		nil,
	)
	if err != nil {
		return
	}
	return
}
