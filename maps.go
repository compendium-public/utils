package utils

func MergeMap(left, right m) {
	for k, v := range right {
		left[k] = v
	}
}

func MergeMapB(left, right m) {
	for k, v := range right {
		if left[k] == nil {
			left[k] = v
		}
	}
}

type m = map[string]interface{}

// Given two maps, recursively merge right into left, NEVER replacing any key that already exists in left
func MergeMapDeep(left, right m) m {
	for key, rightVal := range right {
		if leftVal, present := left[key]; present {
			left[key] = MergeMapDeep(leftVal.(m), rightVal.(m))
		} else {
			// key not in left so we can just shove it in
			left[key] = rightVal
		}
	}
	return left
}
