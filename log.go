package utils

import (
	"log"
	"sync"

	"github.com/chappjc/logrus-prefix"
	"github.com/sirupsen/logrus"
)

type UtilLog struct {
	*logrus.Entry
}

var (
	globalLogger   *logrus.Logger
	getLoggerMutex sync.Mutex
)

func NewLogger(prefix string) (log UtilLog) {
	if prefix == "" {
		prefix = "<no prefix>"
	}
	if globalLogger == nil {
		getLoggerMutex.Lock()
		defer getLoggerMutex.Unlock()
		logger := logrus.New()
		logger.SetFormatter(&prefixed.TextFormatter{
			FullTimestamp: true,
		})
		globalLogger = logger
	}

	log.Entry = globalLogger.WithField("prefix", prefix)
	
	return log
}

func (l *UtilLog) HasErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func GlobalLoggerAddHook(hook logrus.Hook) {
	globalLogger.AddHook(hook)
}

func GlobalLoggerSetLevelDebug() {
	globalLogger.SetLevel(logrus.DebugLevel)
}

func GlobalLoggerSetLevelInfo() {
	globalLogger.SetLevel(logrus.InfoLevel)
}

func GlobalLoggerSetLevelWarn() {
	globalLogger.SetLevel(logrus.WarnLevel)
}