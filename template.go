package utils

import (
	"bytes"
	templateHTML "html/template"
	"io/ioutil"
	"os"
	"text/template"
	"time"

	"gopkg.in/yaml.v3"
)

var (
	globalTextFuncMap = template.FuncMap{
		"env": func(key string) string {
			return os.Getenv(key)
		},
		"safeHTML": func(v string) templateHTML.HTML {
			return templateHTML.HTML(v)
		},
		"copy": func() string {
			return time.Now().Format("2006")
		},
		"time": func(t time.Time) string {
			return t.Format("2006-01-02 15:04:05")
		},
		"timeformat": func(t time.Time, format string) string {
			return t.Format(format)
		},
		"now": time.Now,
	}
	globalHTMLFuncMap = templateHTML.FuncMap{
		"env": func(key string) string {
			return os.Getenv(key)
		},
		"safeHTML": func(v string) templateHTML.HTML {
			return templateHTML.HTML(v)
		},
		"copy": func() string {
			return time.Now().Format("2006")
		},
		"time": func(t time.Time) string {
			return t.Format("2006-01-02 15:04:05")
		},
		"timeformat": func(t time.Time, format string) string {
			return t.Format(format)
		},
		"now": time.Now,
	}
)

func TemplateAddGlobalFunc(k string, v interface{}) {
	globalTextFuncMap[k] = v
	globalHTMLFuncMap[k] = v
}

func TemplateString(text string, data interface{}) (string, error) {
	tmpl, err := template.New("test").Funcs(globalTextFuncMap).Parse(text)
	if err != nil {
		panic(err)
	}
	var tpl bytes.Buffer
	err = tmpl.Execute(&tpl, data)

	return tpl.String(), err
}

func TemplateFileToString(file string, data interface{}) (string, error) {
	text, err := ioutil.ReadFile(file)
	if err != nil {
		return "", err
	}

	return TemplateString(string(text), data)
}

func TemplateFileToBytes(file string, data interface{}) ([]byte, error) {
	text, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	tmpl, err := template.New("test").Funcs(globalTextFuncMap).Parse(string(text))
	if err != nil {
		return nil, err
	}
	var tpl bytes.Buffer
	err = tmpl.Execute(&tpl, data)

	return tpl.Bytes(), err
}

func LoadTemplatedYaml(file string, tvars map[string]interface{}, data interface{}) (err error) {
	source, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	t, err := template.New("default").Funcs(globalTextFuncMap).Parse(string(source))
	if err != nil {
		return err
	}

	var tpl bytes.Buffer
	err = t.Execute(&tpl, tvars)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(tpl.Bytes(), data)
	if err != nil {
		return err
	}
	return
}
