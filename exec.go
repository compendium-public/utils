package utils

import (
	"bytes"
	"io"
	"os"
	"os/exec"
)

var (
	defaultExec = ExecCommandWithLocalOutput
)

func ExecSetDefaultNoOut() {
	defaultExec = ExecCommandNoOut
}

func ExecSetDefaultWithOut() {
	defaultExec = ExecCommandWithLocalOutput
}

func ExecCommand(dir, command string, args, env []string) (string, error) {
	return defaultExec(dir, command, args, env)
}

func ExecCommandWithLocalOutput(dir, command string, args, env []string) (string, error) {
	cmd := exec.Command(command, args...)

	cmd.Env = env
	cmd.Dir = dir

	var out bytes.Buffer
	cmd.Stdout = io.MultiWriter(os.Stdout, &out)
	cmd.Stderr = io.MultiWriter(os.Stderr, &out)

	err := cmd.Run()
	if err != nil {
		return out.String(), err
	}

	return out.String(), nil
}

func ExecCommandNoOut(dir, command string, args, env []string) (string, error) {
	cmd := exec.Command(command, args...)

	cmd.Env = env
	cmd.Dir = dir

	out, err := cmd.CombinedOutput()
	if err != nil {
		return string(out), err
	}

	return string(out), nil
}
