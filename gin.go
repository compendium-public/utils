package utils

import (
	"crypto/tls"
	"net/http"

	gintemplate "github.com/foolin/gin-template"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-contrib/location"
	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
)

func NewGinDefault() (r *gin.Engine) {
	return NewGin("./web/assets", "web/views", nil)
}

func NewGin(assets, views string, partials []string) (r *gin.Engine) {
	r = gin.New()

	r.Use(gin.Recovery())
	r.Use(location.Default())
	r.Use(requestid.New())
	r.Use(gzip.Gzip(gzip.DefaultCompression))

	// Use default logger
	r.Use(gin.Logger())

	// Cors Config
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Origin"}
	r.Use(cors.New(corsConfig))

	//new template engine
	if views != "" {
		r.HTMLRender = gintemplate.New(gintemplate.TemplateConfig{
			Root:         views,
			Extension:    ".html",
			Master:       "layouts/main",
			Partials:     partials,
			Funcs:        globalHTMLFuncMap,
			DisableCache: true,
		})
	}

	if assets != "" {
		r.Static("/assets", assets)
		r.Static("/css", assets+"/css")
		r.Static("/font", assets+"/font")
		r.Static("/img", assets+"/img")
		r.Static("/js", assets+"/js")

		r.StaticFile("/favicon.ico", assets+"/favicon.ico")
	}

	return
}

func GinRun(r *gin.Engine, addr string) {
	r.Run(addr)
}

func GinRunSelfSignedSSL(r *gin.Engine, addr string) (err error) {
	certs, err := GenerateCertificates()
	if err != nil {
		return
	}

	tlsconfig := &tls.Config{
		Certificates: certs,
		NextProtos: []string{
			"h2", "http/1.1", // enable HTTP/2
		},
	}

	s := &http.Server{
		Addr:      addr,
		TLSConfig: tlsconfig,
		Handler:   r,
	}

	return s.ListenAndServeTLS("", "")
}

func GinRunSSL(r *gin.Engine, addr, certsdir string) (err error) {
	certs, err := LoadCertificates(certsdir)
	if err != nil {
		return
	}

	tlsconfig := &tls.Config{
		Certificates: certs,
		NextProtos: []string{
			"h2", "http/1.1", // enable HTTP/2
		},
	}

	s := &http.Server{
		Addr:      addr,
		TLSConfig: tlsconfig,
		Handler:   r,
	}

	return s.ListenAndServeTLS("", "")
}

