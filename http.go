package utils

import (
	"bytes"
	//"encoding/json"
	json "github.com/json-iterator/go"
	"io/ioutil"
	"net/http"
)

func HttpPostJson(url string, request, response interface{}) (err error) {
	postBody, err := json.Marshal(request)
	if err != nil {
		return
	}
	requestBody := bytes.NewBuffer(postBody)

	resp, err := http.Post(url, "application/json", requestBody)
	if err != nil {
		return
	}

	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	resp.Body.Close()

	err = json.Unmarshal(body, &response)
	if err != nil {
		return
	}

	return
}

func HttpGetJson(url string, response interface{}) (err error) {
	resp, err := http.Get(url)
	if err != nil {
		return
	}

	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	resp.Body.Close()

	err = json.Unmarshal(body, &response)
	if err != nil {
		return
	}

	return
}

func HttpRequestJsonWithHeaders(method, url string, headers map[string]string, request, response interface{}) (err error) {
	postBody, err := json.Marshal(request)
	if err != nil {
		return
	}
	requestBody := bytes.NewBuffer(postBody)

	req, err := http.NewRequest(method, url, requestBody)
	if err != nil {
		return
	}

	for k, v := range headers {
		req.Header.Add(k, v)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	resp.Body.Close()

	err = json.Unmarshal(body, &response)
	if err != nil {
		return
	}

	return
}
