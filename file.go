package utils

import (
	//"encoding/json"
	"io/ioutil"
	"path/filepath"
	"strings"

	json "github.com/json-iterator/go"

	"gopkg.in/yaml.v3"
)

func ReadYaml(file string, data interface{}) (err error) {
	filename, _ := filepath.Abs(file)
	yamlFile, err := ioutil.ReadFile(filename)

	if err != nil {
		return
	}

	err = yaml.Unmarshal(yamlFile, data)

	return
}

func ReadJson(file string, data interface{}) (err error) {
	filename, _ := filepath.Abs(file)
	jsonFile, err := ioutil.ReadFile(filename)

	if err != nil {
		return
	}

	err = json.Unmarshal(jsonFile, data)

	return
}

func WriteYaml(file string, data interface{}) (err error) {
	filename, _ := filepath.Abs(file)

	yamlBytes, err := yaml.Marshal(data)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, yamlBytes, 0644)

	return
}

func WriteJson(file string, data interface{}) (err error) {
	filename, _ := filepath.Abs(file)

	jsonBytes, err := json.Marshal(data)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, jsonBytes, 0644)

	return
}

func FilesAndDirsWithExtention(rootpath, extention string) (files, dirs []string, err error) {
	_files, err := ioutil.ReadDir(rootpath)
	if err != nil {
		return
	}

	for _, info := range _files {
		if info.IsDir() {
			dirs = append(dirs, info.Name())
		} else {
			if filepath.Ext(info.Name()) == extention {
				files = append(files, strings.TrimSuffix(info.Name(), filepath.Ext(info.Name())))
			}
		}
	}

	return
}

func FilesAndDirs(rootpath string) (files, dirs []string, err error) {
	_files, err := ioutil.ReadDir(rootpath)
	if err != nil {
		return
	}

	for _, info := range _files {
		if info.IsDir() {
			dirs = append(dirs, info.Name())
		} else {
			files = append(files, info.Name())
		}
	}

	return
}

func Dirs(rootpath string) (dirs []string, err error) {
	_files, err := ioutil.ReadDir(rootpath)
	if err != nil {
		return
	}

	for _, info := range _files {
		if info.IsDir() {
			dirs = append(dirs, info.Name())
		}
	}

	return
}

func FilesWithExtention(rootpath, extention string) (files []string, err error) {
	_files, err := ioutil.ReadDir(rootpath)
	if err != nil {
		return
	}

	for _, info := range _files {
		if !info.IsDir() {
			if filepath.Ext(info.Name()) == extention {
				files = append(files, strings.TrimSuffix(info.Name(), filepath.Ext(info.Name())))
			}
		}
	}

	return
}

func Files(rootpath string) (files []string, err error) {
	_files, err := ioutil.ReadDir(rootpath)
	if err != nil {
		return
	}

	for _, info := range _files {
		if !info.IsDir() {
			files = append(files, info.Name())
		}
	}

	return
}

func YamlFileMerge(files []string, data interface{}) (err error) {
	d := map[string]interface{}{}
	for _, file := range files {
		d2 := map[string]interface{}{}
		err = ReadYaml(file, &d2)
		if err != nil {
			return
		}
		d = MergeMapDeep(d, d2)
	}

	yamlBytes, err := yaml.Marshal(d)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(yamlBytes, data)

	return
}

func YamlFileTemplateMerge(files []string, tvars map[string]interface{}, data interface{}) (err error) {
	d := map[string]interface{}{}
	for _, file := range files {
		d2 := map[string]interface{}{}
		err = LoadTemplatedYaml(file, tvars, &d2)
		if err != nil {
			return
		}
		d = MergeMapDeep(d, d2)
	}

	yamlBytes, err := yaml.Marshal(d)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(yamlBytes, data)

	return
}
